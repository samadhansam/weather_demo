//
//  AppDelegate.swift
//  Weather_Demo
//
//  Created by samadhan on 29/09/20.
//  Copyright © 2020 weather. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {


    var window: UIWindow?

    

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        self.window = UIWindow.init(frame: UIScreen.main.bounds)
        
        
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = mainStoryboard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
        let navigationVC = UINavigationController.init(rootViewController: viewController)
        self.window!.rootViewController = navigationVC
        self.window!.makeKeyAndVisible()
        
        
        return true
    }
    
    

    

}

